from django.apps import AppConfig


class PokerbuddyConfig(AppConfig):
    name = 'pokerbuddy'
