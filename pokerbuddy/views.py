from django.shortcuts import render
from .models import Carta
from django.http import HttpResponse
from deuces import Card
from deuces import Evaluator
from django.core.mail import send_mail

# Create your views here.

def AllCards(request):
    """TODO: Docstring for AllCards.
    :returns: TODO
    """
    #Deck_list = Carta.objects.all()
    Hearts = Carta.objects.filter(card_suit='h')
    Spades = Carta.objects.filter(card_suit='s')
    Diamonds = Carta.objects.filter(card_suit='d')
    Clubs = Carta.objects.filter(card_suit='c')
    Back = Carta.objects.filter(card_suit='b')
    Community = list(range(5));
    Hold = list(range(2));

    Context = {
        "hearts":Hearts,
        "spades":Spades,
        "diamonds":Diamonds,
        "clubs":Clubs,
        "back":Back,
        "community_cards":Community,
        "hold_cards":Hold,
    }

   # send_mail('Subject here',
   #          'Here is messge',
   #          'lucha0075.db@gmail.com',
   #          ['lucha0075.db@gmail.com'],
   #          fail_silently=False,)

    return render(request, "pokerbuddy/index.html", Context) 

def Evaluation(request, user_cards ):


    user_cards_list =  user_cards.split("-")
    user_cards_list.reverse()
    board = []
    hand = []
    evaluator = Evaluator()

    count = 0;
    for item in user_cards_list:
        count = count + 1
        if count > 2:
            new_card = Card.new(item)
            board.append(new_card)
            pass
        else:
            new_card = Card.new(item)
            hand.append(new_card)
        pass
    user_score = evaluator.evaluate(board, hand)
    user_class = evaluator.get_rank_class(user_score)
    user_class_string = evaluator.class_to_string(user_class)
    #return HttpResponse("Points: {}, Cards: {}.".format(evaluator.evaluate(board, hand), user_cards))
    return HttpResponse("Points: {}, Cards: {}.".format(user_score,user_class_string))

    return render(request, "pokerbuddy/testpage.html", Context)

