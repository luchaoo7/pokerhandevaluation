from django.core.management.base import BaseCommand, CommandError
from pokerbuddy.models import Carta

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'
    
    def add_arguments(self, parser):
        parser.add_argument('id', nargs='+', type=int)

        parser.add_argument('sum',
                            type=int,
                            help='sum the integers (default: find the max)')

    def handle(self, *args, **options):
        if options['sum']:
            card = Carta.objects.get(pk=options['sum'] + 1)
            self.stdout.write(self.style.SUCCESS('Found --sum "%s"' % card.card_image))
            pass
        for card_id in options['id']:
            try:
                card = Carta.objects.get(pk=card_id)
                self.stdout.write(self.style.SUCCESS('Successfully closed card "%s"' % card.card_image))
            except card.DoesNotExist:
                raise CommandError('Poll "%s" does not exist' % card_id)
            
            card.card_password = "daniel brown"
            card.save()
            
            self.stdout.write(self.style.SUCCESS('Successfully closed card "%s"' % card_id))
