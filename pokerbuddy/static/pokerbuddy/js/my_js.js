$(document).ready(function(){

    /*used to set only one card*/
    var bool = true;
    var community_card_selected = 0;
    var hold_card_selected = 0;

    
    /*Select community cards*/
    $(".select_commun").click(function(){
        

        var select_image =  $(this).attr("src");
        var select_id =  $(this).attr("id");
        var commun_image =  "";
        bool = true;


        /*check the second time the user clicks this card
         check if it has been used,  if it hasn't, loop all community slots*/
        var object_classes = $(this).attr("class");
        var is_set = object_classes.includes("used");
        if (!is_set) 
        {
            
            /*loop all community cards*/
            $(".hold_card").each(function(index){

                /*store classes*/
                var object_classes = $(this).attr("class");

                commun_image =  $(this).attr("src");
                /*check if string contains a substring we
             are looking for, in this case 'set'*/
                var is_set = object_classes.includes("set");
                if (!is_set && bool) 
                {
                    bool = false;
                    hold_card_selected++;

                    /*set the image src*/
                    $(this).attr("src", select_image);
                    /*set the id value*/
                    $(this).attr("id", select_id);
                    /*set class*/
                    $(this).addClass("set");
                }
            });
            
            /*loop all community cards*/
            $(".community").each(function(index){

                /*store classes*/
                var object_classes = $(this).attr("class");

                commun_image =  $(this).attr("src");
                /*check if string contains a substring we
             are looking for, in this case 'set'*/
                var is_set = object_classes.includes("set");
                if (!is_set && bool) 
                {
                    bool = false;
                    community_card_selected++;

                    /*set the image src*/
                    $(this).attr("src", select_image);
                    /*set the id value*/
                    $(this).attr("id", select_id);
                    /*set class*/
                    $(this).addClass("set");
                }
            });

        }

        //$(this).attr("src", "/static/pokerbuddy/images/small/back_of_card.png");
        /*This means this card has been used, 
        face the card down*/
        if (!bool) 
        {
            $(this).addClass("used");
            $(this).attr("src", commun_image);
            //$(this).attr("id", "");

            var twin_card_class = object_classes.substring(0,2);

            /*loop all twin classes and set them*/
            flip_twin_card(twin_card_class, commun_image);
            
        }

        /*5 represents the amount of cards flipped*/
        hide_show_button(community_card_selected, hold_card_selected);

    });
    
    
    /*Select hold cards*/
    $(".select_hold").click(function(){


        var parent = $(this);
        var select_image =  $(this).attr("src");
        var select_id =  $(this).attr("id");
        var commun_image =  "";
        bool = true;


        /*check the second time the user clicks this card
         check if it has been used,  if it hasn't, loop all community slots*/
        var object_classes = $(this).attr("class");
        var is_set = object_classes.includes("used");
        if (!is_set) 
        {

            /*loop all hold cards*/
            $(".hold").each(function(index){

                /*store classes*/
                var object_classes = $(this).attr("class");

                commun_image =  $(this).attr("src");
                /*check if string contains a substring we
             are looking for, in this case 'set'*/
                var is_set = object_classes.includes("set");
                if (!is_set && bool) 
                {
                    parent.attr("id", "");

                    bool = false;

                    /*set the image src*/
                    $(this).attr("src", select_image);
                    /*set the id value*/
                    $(this).attr("id", select_id);
                    /*set class*/
                    $(this).addClass("set");
                }
            });

        }

        /*This means this card has been used, 
        face the card down*/
        if (!bool) 
        {
            $(this).addClass("used");
            $(this).attr("src", commun_image);
            $(this).attr("id", "");
            var twin_card_class = object_classes.substring(0,2);

            /*loop all twin classes and set them*/
            flip_twin_card(twin_card_class, commun_image);
        }

    });


    /*button to evaluate cards*/
    $("#evaluate").click(function(){

        /*Get the id from the cards that are set*/
        var data = "";
        var count = 0;
        $("form > img").each(function(index){
            //alert($(this).attr("id"));
            if ($(this).attr("id")) 
            {
                count++;
                data += (count > 1) ? "-"+$(this).attr("id") :
                    $(this).attr("id");
            }
        });


        $.ajax({url: "/evaluation/"+data, success: function(result){
            $("#div1").html(result);
        }});



    })
    
    
    /*button to refresh the cards*/
    $("#refresh").click(function(){
        $.ajax({url: "/cards", success: function(result){
            $("#main_div").html(result);

        }});

    })


    /*Set community img cards back to back_of_deck when clicked*/
    /*Does the same as function "turn_community_card"*/
    /*
    flip community and hold card to back side*/
    $(".community").click(function(){
        var current_id = $(this).attr("id");
        var current_src = $(this).attr("src");


        $(this).attr("src","/static/pokerbuddy/images/small/back_of_card.png");
        bool = true;
        $(this).removeClass("set");
        id_len = $(this).attr("id").length;
        $(this).attr("id", "");

        if (id_len > 0) 
        {
            
            /*if we are here, the selected cards have been turned
            back down, and the selectoin section's card turn back up*/
            selection_card_object = $(".selection1_div").find("."+current_id);
            selection_card_object.attr("src", current_src);
            selection_card_object.removeClass("used");
            //selection_card_object.attr("src", );


            if (!community_card_selected <= 0) 
            {
                community_card_selected--;
                hide_show_button(community_card_selected, hold_card_selected);
            }
        }
    });

    /*Set hold img cards back to back_of_deck when clicked*/
    /*Does the same as function "turn_community_card"*/
    /*
    flip community and hold card to back side*/
    $(".hold_card").click(function(){
        var current_id = $(this).attr("id");
        var current_src = $(this).attr("src");


        $(this).attr("src","/static/pokerbuddy/images/small/back_of_card.png");
        bool = true;
        $(this).removeClass("set");
        id_len = $(this).attr("id").length;
        $(this).attr("id", "");

        if (id_len > 0) 
        {
            /*if we are here, the selected cards have been turned
            back down, and the selectoin section's card turn back up*/
            selection_card_object = $(".selection1_div").find("."+current_id);
            selection_card_object.attr("src", current_src);
            selection_card_object.removeClass("used");
            //selection_card_object.attr("src", );


            if (!hold_card_selected <= 0) 
            {
                hold_card_selected--;
                hide_show_button(community_card_selected, hold_card_selected);
            }
        }
    });




});


function flip_twin_card(class_lookup, new_image)
{
    var x = document.getElementsByClassName(class_lookup);
    var i;
    for (i = 0; i < x.length; i++) 
    {
        x[i].src = new_image;
        //x[i].id = "";
        x[i].classList.add("used");
    } 
}

/*can use this fuction, on the onclick method of the images img*/
function turn_community_card(obj) 
{
    /*Set community img cards back to back_of_deck when clicked*/
    $(".community").click(function(){
        $(this).attr("src","/static/pokerbuddy/images/small/back_of_card.png");
        bool = true;
        $(this).removeClass("set");
        $(this).attr("id", "");
    });
}


/*onmouseover set image 1st card*/
function turn_hold_card(obj) 
{
    $(".hold").click(function(){
        $(this).attr("src","/static/pokerbuddy/images/small/back_of_card.png");
        bool = true;
        $(this).removeClass("set");
        $(this).attr("id", "");
    });
}

function hide_show_button(community_cards, hold_cards) 
{

    //$("#evaluate").css("display","inline");
    if (hold_cards < 2) 
    {
        $("#evaluate").css("display","none");
    }
    else if (community_cards < 3) 
    {
        $("#evaluate").css("display","none");
    }
    else if ((community_cards + hold_cards ) >= 5) 
    {
        $("#evaluate").css("display","inline");
    }
    else
    {
        $("#evaluate").css("display","none");
    }
}



