from django.db import models

# Create your models here.

class Carta( models.Model ):

    """Docstring for Carta. """

    DIAMONDS = 'd'
    CLUBS = 'c'
    HEARTS = 'h'
    SPADES = 's'
    BACK = 'b'
    SUIT = (
        (DIAMONDS, 'Diamonds'),
        (CLUBS, 'Clubs'),
        (HEARTS, 'Hearts'),
        (SPADES, 'Spades'),
        (BACK, 'Back'),
    )

    BACK = 'b'
    ACE = '1'
    TWO = '2'
    THREE = '3'
    FOUR = '4'
    FIVE = '5'
    SIX = '6'
    SEVEN = '7'
    EIGHT = '8'
    NINE = '9'
    TEN = 'T'
    JACK = 'J'
    QUEEN = 'Q'
    KING = 'K'
    NUMBER = (
        (BACK, 'Back'),
        (ACE, 'Ace'),
        (TWO, 'Two'),
        (THREE, 'Three'),
        (FOUR, 'Four'),
        (FIVE,'Five'),
        (SIX, 'Six'),
        (SEVEN, 'Seven'),
        (EIGHT, 'Eight'),
        (NINE, 'Nine'),
        (TEN, 'Ten'),
        (JACK, 'Jack'),
        (QUEEN, 'Queen'),
        (KING, 'King'),
    ) 

    card_number = models.CharField(max_length=1, choices=NUMBER)
    card_suit = models.CharField(max_length=1, choices=SUIT)
    card_password = models.CharField(max_length=250, default="")
    card_image = models.CharField(max_length=250)
    card_id = models.CharField(max_length=2, verbose_name="NATURAL-ID", default="")
    card_testfield = models.CharField(max_length=2, verbose_name="test-field", default="")


    def __str__(self):
        """TODO: Docstring for __str__.
        :returns: TODO

        """
        return self.card_number + " " + self.card_suit
