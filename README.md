Introduction 
---------
Poker hand evaluation allows you to determine the strength of the cards you pick -- this is
the combination of the 2 hold cards and the 3 to 5 community cards.

### Setup steps
#### Getting Django ready
Download the django project.
```
git clone git@gitlab.com:luchaoo7/pokerhandevaluation.git
```
Change directory into downloaded project.
```
cd pokerhandevaluation
```
Create a python virtual environment. This isolates your python environment from your systems python environment.
You two was of accomplishing this. 
```
python3 -m venv venv
or
virtualenv -p /usr/bin/python3 venv
```
Activate your python environment.
```
source venv/bin/activate
```
Install the python modules needed to run the project.
```
pip install -r requirements.txt
```

#### Preparing database for data
Create the relevant migrations to be applied later 
```
python manage.py makemigrations
```
Apply changes to database
```
python manage.py migrate
```
Load data into your database
```
python manage.py loaddata setup_data.json
```

#### Running Django
Run the django project 
```
python manage.py runserver
```
This should start a django and you should be able to access it by going to:
- <http://localhost:8000/cards>











